#include "msp430g2553.h"
#include "Joystick.h"

void printf(char *format, ...);

Joystick joystick;

void initializeTimer() {
    TA0CTL = TASSEL_2 + ID_0 + MC_1;    // Timer A0 with ACLK, count UP
    TA0CCR0 = 1;

    TA0CCTL0 = 0x10;                    // Enable Timer A0 interrupts
}

void directionChanged(char direction) {
    printf("DIRECTION:%i\n", (int) direction);
}

void btnClicked() {
    printf("BUTTON\n");
}

void backPressed() {
    printf("BACK\n");
}

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;

    joystick.init();
    joystick.onDirectionChanged(&directionChanged);
    joystick.onButtonPressed(&btnClicked);
    joystick.onBackPressed(&backPressed);

    initializeTimer();
    __bis_SR_register(GIE);                       // Enable Interrupt

    while(1);
}

#pragma vector = TIMER0_A0_VECTOR                // Timer0_A0 ISR
__interrupt void Timer0_A0 (void) {

    joystick.controlADC();
    joystick.controlButton();

}


